#include "functions.hpp"
#include <math.h>
#include <fstream>
#include <iostream>

namespace el
{
    bool isPrime(int x)
    {
        if (x < 2)
            return false;
        for (int d = 2; d <= sqrt(x); d++)
            if (x % d == 0)
                return false;
        return true;
    }

    void Read(int& n, int& m, int arr[100][100])
    {
        std::cin >> n >> m;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                std::cin >> arr[i][j];
    }

    bool ConsistsPrime(int n, int m, int arr[100][100])
    {

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if (isPrime(arr[i][j]))
                    return true;
        return false;
    }

    void Write(int n, int m, int arr[100][100])
    {
        std::cout << "���������: " << std::endl;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                std::cout << arr[i][j] << " ";
            }
            std::cout << "\n";
        }
    }

    bool NulSum(int n, int m, int arr[100][100])
    {
        int i, j, k, sum = 0;
        for (i = 0; i < n; i++)
            for (j = 0; j < m; j++)
            {
                sum += arr[i][j];
            }
        if (sum == 0)
            return true;
        return false;
    }

    bool ProverkaUsloviya(int n, int m, int arr[100][100])
    {
        int i, j, sum = 0;
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < m; j++)
            {
                if (isPrime(arr[i][j]))
                {
                    for (j = 0; j < m; j++)
                        sum += arr[i][j];
                }
            }
            if (sum == 0)
                return true;
            break;
        }
        return false;
    }

    void Sort(int n, int m, int arr[100][100])
    {
        int sum[100];
        for (int j = 0; j < m; j++)
        {
            for (int k = j + 1; k < m; ++k)
            {
                for (int i = 0; i < n-1; i++)
                {
                    sum[j] += arr[i][j];                   
                }
                if (sum[j] > sum[k])
                {
                    for (int i = 0; i < n-1; i++)
                        std::swap(arr[i][j], arr[i][k]);
                }
            }           
        }

    }
}