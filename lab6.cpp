﻿#include <iostream>
#include "functions.hpp"

int main()
{
    setlocale(LC_ALL, "Rus");
    std::cout<<"Введите количество строк и столбцов: ";
    int n, m;
    int arr[100][100];
    el::Read(n, m, arr);
    if (el::ProverkaUsloviya(n, m, arr))
    {
        el::Sort(n, m, arr);
        el::Write(n, m, arr);
    }
    return 0;

}


