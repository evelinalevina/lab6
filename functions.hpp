#pragma once

namespace el
{
    bool isPrime(int x);
    
    void Read(int& n, int& m, int arr[100][100]);
    
    bool ConsistsPrime(int n, int m, int arr[100][100]);
    
    void Write(int n, int m, int arr[100][100]);

    bool NulSum(int n, int m, int arr[100][100]);

    bool ProverkaUsloviya(int n, int m, int arr[100][100]);

    void Sort(int n, int m, int arr[100][100]);
   
}